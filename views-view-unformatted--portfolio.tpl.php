<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php $count =0; foreach ($rows as $id => $row): $count++; $additional_class = $count % 4 == 0 ? ' no-right-margin' : '';?>
  <div<?php if ($classes_array[$id]) print ' class="' . $classes_array[$id] .$additional_class.'"';  ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
