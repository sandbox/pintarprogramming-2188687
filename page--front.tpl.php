<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

<header>
    <?php if ($main_menu): ?>
   	<nav class="top-bar" data-topbar>
		<ul class="title-area"> 
			    <?php if ($logo): ?>

			    	<li class="name">

				      <h1><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
				          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				        </a></h1>
				    </li>
    
	 
    <?php endif; ?>
			<li class="toggle-topbar "><a class="menu-icon" href="#"><span></span></a></li>
		</ul>
		<section class="top-bar-section">
        <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('class' => array('top-navigation')))); ?>
        </section>
    </nav>
    <?php endif; ?>
</header>



  
    <?php if(!is_null($slides)): ?>
    <div class="slider-wrapper" >
    	<div class="slider-background">
    		<span class="white-space"></span>
    	</div>
    	<div class="container-slider">
			<ul class="slider" data-orbit>
				<?php $count = 0; ?>
				<?php foreach($slides as $slide): ?>
					<li data-orbit-slide="<?php echo 'slide-'.$count; ?>">
						<img class="slider-img" src="<?php echo $slide['img']; ?>" />
						
						<div class="orbit-caption">
				      		<?php echo $slide['body']; ?>
				    	</div>
					</li>
					<?php $count++; ?>
				<?php endforeach;?>

			</ul>
		</div>
	</div>
<!--
    <div class="slider-wrapper" >
		<div class="slider flexslider" id="slider" >
			<ul class="slides">
				
			</ul>	
		
			<span class="slider-previous-button">&nbsp</span>
			<span class="slider-next-button">&nbsp</span>
		</div><!-- end of .slider -->
		
	<!--</div><!-- end of .slider-wrapper -->

	
	 <div class="carousel-wrapper" >
	 	<div id="carousel">
			<ul class="slider-thumbnails slides">
				<?php $lastitem =count($slides); $count=0; foreach($slides as $slide): ?>
				<li data-orbit-link="<?php echo "slide-".$count; ?>"  <?php if($count==0) echo 'class="first"'; if($count == $lastitem-1)echo 'class="last"';  ?>><img src="<?php echo $slide['img']; ?>" /></li>
				<?php $count++; endforeach;?>
			</ul><!-- end of .slider-thumbnails -->
			</div>
	 </div><!-- end of .slider-wrapper -->
	<?php endif; ?>
	<div class="content first-page">
		<?php if ($breadcrumb): ?>
	    	<div id="breadcrumb"><?php print $breadcrumb; ?></div>
	    <?php endif; ?>
	    <?php print $messages; ?>
        <?php print render($page['content']); ?>
        <?php $portflio_name = 'latest_portfolio_items';
			$portfolio_view = views_get_view($portflio_name);
			if($portfolio_view){
				//$features_view->set_display('features-view');
				print '<div class="recent-works-heading">
				<h3>'.$portfolio_view->get_title().'</h3>
				<span class="recent-works-headinglines">
				&nbsp;
				</span><!-- end of .recent-works-headinglines -->
				</div><!-- end of .recent-works-heading -->';
				
				print $portfolio_view->preview();
				
			}
		?>
    </div>

    <footer>
    	<div class="footer-background">
    		<span class="white-space">
    		</span>
    	</div>
		<div class="footer-wrapper">
      		<?php print render($page['footer']); ?>
    	</div><!-- end of .footer-wrapper -->
	</footer>

<?php echo '<script src="'.path_to_theme().'/bower_components/foundation/js/foundation.min.js"></script>'; ?>
<script>$(document).foundation({
  orbit: {
    animation: 'slide',
    timer_speed: 1000,
    pause_on_hover: true,
    animation_speed: 500,
    navigation_arrows: true,
    bullets: false,
    slide_number: false
  }
});
</script>
<?php //echo '<script src="'.path_to_theme().'/js/jquery.flexslider.js"></script>'; ?>
<?php //echo '<script src="'.path_to_theme().'/js/carousel_starter.js"</script>'; ?>
		
		
