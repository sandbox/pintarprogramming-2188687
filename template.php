<?php

/**
 * Add body classes if certain regions have content.
 */
function jetro_preprocess_html(&$variables) {
  //drupal_add_js(path_to_theme() . '/js/jquery.flexslider.js', array('scope' => 'footer'));
  //drupal_add_js(path_to_theme() . '/js/carousel_starter.js', array('scope' => 'footer'));
  // Add conditional stylesheets for IE
  //drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 9', '!IE' => FALSE), 'preprocess' => FALSE));  
}

function jetro_preprocess_page(&$variables) {
  if(drupal_is_front_page()) {
    $slides = node_load_multiple(array(), array('type' => 'slide'));
	$count = 0;
	foreach ($slides as $slide){
	  $body = field_get_items('node', $slide, 'body');
	  $img = field_get_items('node', $slide, 'field_img');
	  $variables['slides'][$count]['body']=$body[0]['value'];
	  $variables['slides'][$count]['img']=file_create_url($img[0]['uri']);
	  $count++;
	}
  }
  $variables['show_sidebar'] = false;
  // show sidebar on blog and blog posts
  /*
  if(isset($variables['node']) && ($variables['node']->type == 'blog' || 
    $variables['node']->type == 'blog_post' ||
	$variables['node']->type == 'webform') ){
	$variables['show_sidebar'] = true;
  }*/
}



function jetro_menu_tree__main_menu($variables){
  return '<ul class="left">' . $variables['tree'] . '</ul>';
}

/**
 * Override or insert variables into the page template.
 */
function jetro_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function jetro_preprocess_maintenance_page(&$variables) {
  // By default, site_name is set to Drupal if no db connection is available
  // or during site installation. Setting site_name to an empty string makes
  // the site and update pages look cleaner.
  // @see template_preprocess_maintenance_page
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  drupal_add_css(drupal_get_path('theme', 'jetro') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function jetro_process_maintenance_page(&$variables) {
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the node template.
 */
function jetro_preprocess_node(&$variables) {
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
  $variables['show_sidebar'] = false;
  // show sidebar on blog and blog posts
  if(isset($variables['node']) && ($variables['node']->type == 'blog' ||
  	$variables['node']->type == 'blog_post') ||
    $variables['node']->type == 'webform'){
  	$variables['show_sidebar'] = true;
  }  
}

/**
 * Override or insert variables into the block template.
 */
function jetro_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}


/**
 * Implements theme_field__field_type().
 */
function jetro_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

  return $output;
}

/**
 * Display the simple view of rows one after another
 */
function jetro_preprocess_views_view_unformatted(&$vars) {
  $view = $vars['view'];
  $rows = $vars['rows'];
  $style = $view->style_plugin;
  $options = $style->options;

  $vars['classes_array'] = array();
  $vars['classes'] = array();
  $default_row_class = isset($options['default_row_class']) ? $options['default_row_class'] : FALSE;
  $row_class_special = isset($options['row_class_special']) ? $options['row_class_special'] : FALSE;
  // Set up striping values.
  $count = 0;
  $max = count($rows);
  foreach ($rows as $id => $row) {
	$count++;
	// SPECIAL CASE for jetro theme
	if($view->name == 'latest_portfolio_items'){
      if($count == 1)
	    $vars['classes'][$id][] = 'first-work-item';
	  else if($count == 4)
		$vars['classes'][$id][] = 'last-work-item';
  	  else
		$vars['classes'][$id][] = 'work-item';
	}
	//
	if ($default_row_class) {
	  $vars['classes'][$id][] = 'views-row';
	  $vars['classes'][$id][] = 'views-row-' . $count;
	}
	if ($row_class_special) {
	  $vars['classes'][$id][] = 'views-row-' . ($count % 2 ? 'odd' : 'even');
	  if ($count == 1) {
		$vars['classes'][$id][] = 'views-row-first';
      }
	  if ($count == $max) {
		$vars['classes'][$id][] = 'views-row-last';
	  }
	}

	if ($row_class = $view->style_plugin->get_row_class($id)) {
	  $vars['classes'][$id][] = $row_class;
	}
	// Flatten the classes to a string for each row for the template file.
	$vars['classes_array'][$id] = isset($vars['classes'][$id]) ? implode(' ', $vars['classes'][$id]) : '';
  }
}

function jetro_preprocess_views_view_fields(&$vars) {
  $view = $vars['view'];
	
  // Loop through the fields for this view.
  $previous_inline = FALSE;
  $vars['fields'] = array(); // ensure it's at least an empty array.
  foreach ($view->field as $id => $field) {
    // render this even if set to exclude so it can be used elsewhere.
	$field_output = $view->style_plugin->get_field($view->row_index, $id);
	$empty = $field->is_value_empty($field_output, $field->options['empty_zero']);
	if (empty($field->options['exclude']) && (!$empty || (empty($field->options['hide_empty']) && empty($vars['options']['hide_empty'])))) {
	  $object = new stdClass();
	  $object->handler = &$view->field[$id];
	  $object->inline = !empty($vars['options']['inline'][$id]);
	  $object->element_type = $object->handler->element_type(TRUE, !$vars['options']['default_field_elements'], $object->inline);
	  if ($object->element_type) {
	    $class='';
		if ($object->handler->options['element_default_classes']) {
		  $class = 'field-content';
		}

		if ($classes = $object->handler->element_classes($view->row_index)) {
		  if ($class) {
		    $class .= ' ';
		  }
		  $class .=  $classes;
		}

		$pre = '<' . $object->element_type;
				
		if ($class) {
		  $pre .= ' class="' . $class . '"';
		}
				
		$field_output = $pre . '>' . $field_output . '</' . $object->element_type . '>';
      }
	  // Protect ourself somewhat for backward compatibility. This will prevent
	  // old templates from producing invalid HTML when no element type is selected.
	  if (empty($object->element_type)) {
	    $object->element_type = 'span';
	  }

	  $object->content = $field_output;
	    if (isset($view->field[$id]->field_alias) && isset($vars['row']->{$view->field[$id]->field_alias})) {
		  $object->raw = $vars['row']->{$view->field[$id]->field_alias};
		}
		else {
		  $object->raw = NULL; // make sure it exists to reduce NOTICE
		}

		if (!empty($vars['options']['separator']) && $previous_inline && $object->inline && $object->content) {
		  $object->separator = filter_xss_admin($vars['options']['separator']);
		}

		$object->class = drupal_clean_css_identifier($id);

		$previous_inline = $object->inline;
		$object->inline_html = $object->handler->element_wrapper_type(TRUE, TRUE);
		if ($object->inline_html === '' && $vars['options']['default_field_elements']) {
		  $object->inline_html = $object->inline ? 'span' : 'div';
		}

		// Set up the wrapper HTML.
		$object->wrapper_prefix = '';
		$object->wrapper_suffix = '';

		if ($object->inline_html) {
		  $class = '';
		  if ($object->handler->options['element_default_classes']) {
		    $class = "views-field views-field-" . $object->class;
		}

		if ($classes = $object->handler->element_wrapper_classes($view->row_index)) {
		  if ($class) {
		    $class .= ' ';
		  }
		$class .= $classes;
		}

		$object->wrapper_prefix = '<' . $object->inline_html;
		if ($class) {
		  $object->wrapper_prefix .= ' class="' . $class . '"';
		}
		$object->wrapper_prefix .= '>';
		$object->wrapper_suffix = '</' . $object->inline_html . '>';
	  }

	  // Set up the label for the value and the HTML to make it easier
	  // on the template.
      $object->label = check_plain($view->field[$id]->label());
	  $object->label_html = '';
	  if ($object->label) {
	    $object->label_html .= $object->label;
		if ($object->handler->options['element_label_colon']) {
		  $object->label_html .= ': ';
		}

		$object->element_label_type = $object->handler->element_label_type(TRUE, !$vars['options']['default_field_elements']);
		if ($object->element_label_type) {
		  $class = '';
		  if ($object->handler->options['element_default_classes']) {
		    $class = 'views-label views-label-' . $object->class;
		  }

		  $element_label_class = $object->handler->element_label_classes($view->row_index);
		  if ($element_label_class) {
		    if ($class) {
			  $class .= ' ';
			}

		    $class .= $element_label_class;
		  }

		  $pre = '<' . $object->element_label_type;
		  if ($class) {
		    $pre .= ' class="' . $class . '"';
		  }
		  $pre .= '>';

		  $object->label_html = $pre . $object->label_html . '</' . $object->element_label_type . '>';
		}
	  }
	  $vars['fields'][$id] = $object;
    }
  }
}
function jetro_form_alter(&$form, &$form_state, $form_id) {
  if(isset($form['#node']) && $form['#node']->type == 'webform')
    $form['submitted']['message']['#title_display'] ='none';
}

function jetro_form(&$variables) {
  $element = $variables['element'];
  if (isset($element['#action'])) {
    $element['#attributes']['action'] = drupal_strip_dangerous_protocols($element['#action']);
	if(isset($element['#node']) && $element['#node']->type == 'webform'){
	  $element['#attributes']['class'] = 'contact-form';
	  $variables['element']['submitted']['first_name']['#attributes']['placeholder'] =  array('placeholder'=>'Your name');
	}
  }
  if(isset($element['#node'])  && $element['#node']->type == 'webform')
    element_set_attributes($element, array('method', 'id','class'));
  else
	element_set_attributes($element, array('method', 'id'));
  if (empty($element['#attributes']['accept-charset'])) {
	$element['#attributes']['accept-charset'] = "UTF-8";
  }
  // Anonymous DIV to satisfy XHTML compliance.
  return '<form' . drupal_attributes($element['#attributes']) . '><div>' . $element['#children'] . '</div></form>';
}
