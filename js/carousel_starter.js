
$(window).load(function() {
  // The slider being synced must be initialized first
  var itemWidth = ($('#carousel').width()-120)/5;
  var showDirectionNav = false;
  if(itemWidth < 90){
	  itemWidth = ($('#carousel').width()-45)/2;
	  showDirectionNav = true;
  }
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: itemWidth,
    itemMargin: 30,
    directionNav: showDirectionNav,
    useCSS: true,
    asNavFor: '#slider'
  
  });
  $('#carousel .slides > li').css('display','block');
  $('#carousel .slides > li').css('float','left');
  $('#carousel .slides > li').css('width',itemWidth);
  $('#carousel .slides > li').show();
  
  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    directionNav: false,
    slideshow: false,
    sync: "#carousel",
    itemMargin: 0
  
  });
  $('#slider .slides > li').css('display','block');
  $('#slider .slides > li').css('float','left');
  $('#slider .slides > li').width($('#carousel').width());
  $('#slider .slides > li').show();
  //$('.carousel-wrapper').show();
  $('.slider-previous-button').on('click', function(){
	    $('#slider').flexslider('prev');
	    return false;
	});

	$('.slider-next-button').on('click', function(){
	    $('#slider').flexslider('next');
	    return false;
	});
	var sliderImgWidth = $('#slider').width();
    var sliderWidth = 9400;//$('#slider .slides').width();

	$('.slide-info').each(function(i,val){
		$(val).css('width',(450/sliderWidth*100)+'%');
		if(i == 0)
			$(val).css('left', ((20/sliderWidth)*100)+'%');
		else{
			$(val).css('left', (( ((((i)*sliderImgWidth)+20))/sliderWidth))*100+'%');
		}
	});
	
});