// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs


function properSizeDecoration(targetElem, containerElem){
	var containerWidth = containerElem.width();
	var windowWidth = $(document).width();
	var offset = 11;
	if(windowWidth - containerWidth < (offset * 2 + 2)){
		targetElem.hide();
		return;
	}
	targetElem.width((windowWidth - containerWidth)/2 - offset);
	targetElem.show();
}

function setMaxWidth(containerElem,targetElem,widthTaken,offset,show){
	if(containerElem.width() <= widthTaken + offset){
		targetElem.hide();
		return;
	}
	targetElem.width((containerElem.width() - widthTaken - offset));
	if(show)
		targetElem.show();
}

$( window ).resize(function() {
	
	var sidebar = $('.sidebar');
	if(sidebar !== undefined){
		$('.sidebar-widget').each(function(i,val){
			setMaxWidth(sidebar,$(val).find('.headinglines'),$(val).find('h3').width()+25,0,false);
		});
	}
	setMaxWidth($('.recent-works-heading'),$('.recent-works-headinglines'),$('.recent-works-heading h3').width(),20,true);
});

$(document).ready(function () {
	//$(document).foundation();

	setMaxWidth($('.recent-works-heading'),$('.recent-works-headinglines'),$('.recent-works-heading h3').width(),20,true);
	var sidebar = $('.sidebar');
	if(sidebar !== undefined){
		$('.sidebar-widget').each(function(i,val){
			setMaxWidth(sidebar,$(val).find('.headinglines'),$(val).find('h3').width()+25,0,false);
		});
	}

});

